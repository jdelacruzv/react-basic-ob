const contacts = [
	{
		fullname: "Carlos Morales",
		email: "cmorales@yahoo.com",
		telephone: "23456178",
		connected: false,
	},
	{
		fullname: "Ana Fernandez",
		email: "afernandez@hotmail.com",
		telephone: "984523648",
		connected: true,
	},
	{
		fullname: "Pedro Torres",
		email: "ptorres@gmail.com",
		telephone: "123456897",
		connected: false,
	},
];

export default contacts;