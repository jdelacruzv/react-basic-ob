import React from "react";
import "./App.css";
import ContactList from "./components/ContactList";

function App() {
	return (
		<div className="app">
			<h1 className="title-app">Contacts</h1>
			<ContactList />
		</div>
	);
};

export default App;
