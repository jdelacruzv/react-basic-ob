import React from "react";

const ContactCard = ({ contact, changeState, remove }) => {
	return (
		<div className="contact-container">
			<h3>{contact.fullname}</h3>
			<span>{contact.email}</span>
			<span>{contact.telephone}</span>			
			<div className="buttons-container">
				<button
					className="state-button"
					style={{ backgroundColor: contact.status ? "green" : "grey" }}
					onClick={() => changeState(contact)}
				>
					{contact.estado ? "Connected" : "Disconnected"}
				</button>
				<button 
					className="remove-button" 
					onClick={() => remove(contact)}
				>
					Delete
				</button>
			</div>
		</div>
	);
};

export default ContactCard;
