import React, { useState } from "react";
import contacts from "../models/contact";
import ContactForm from "./ContactForm";
import ContactCard from "./ContactCard";

const ContactList = () => {
	const [newContact, setNewContact] = useState(contacts);

	const changeState = (contact) => {
		const index = newContact.indexOf(contact);
		const tempContact = [...newContact];
		tempContact[index].status = !tempContact[index].status;
		setNewContact(tempContact);
	};

	const removeContact = (contact) => {
		const index = newContact.indexOf(contact);
		const tempContact = [...newContact];
		tempContact.splice(index, 1);
		setNewContact(tempContact);
	};

	const addContact = (contact) => {
		const tempContact = [...newContact];
		tempContact.push(contact);
		setNewContact(tempContact);
	};

	const showContactList = newContact.map((contact, index) => {
		return (
			<ContactCard
				key={index}
				contact={contact}
				changeState={changeState}
				remove={removeContact}
			/>
		);
	});

	return (
		<>
			<div className="list-container">
				{showContactList}
			</div>
			<ContactForm onAddContact={addContact}></ContactForm>
		</>
	);
};

export default ContactList;