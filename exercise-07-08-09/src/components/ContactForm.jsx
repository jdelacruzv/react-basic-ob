import React, { useRef } from "react";

const ContactForm = ({ onAddContact }) => {
	const fullname = useRef("");
	const email = useRef("");
	const telephone = useRef("");

	const addContact = (event) => {
		event.preventDefault();

		const newContact = {
			fullname: fullname.current.value,
			email: email.current.value,
			telephone: telephone.current.value,
			connected: true,
		};

		onAddContact(newContact);
		fullname.current.value = "";
		email.current.value = "";
		telephone.current.value = "";
	}

	return (
		<form onSubmit={addContact} className="contact-container">
			<h3>Add Contact</h3>
			<input
				className="form-control mb-2"
				ref={fullname}
				name="fullname"
				type="text"
				placeholder="Fullname"
				autoFocus
			/>
			<input
				className="form-control mb-2"
				ref={email}
				name="email"
				type="email"
				placeholder="Email"
			/>
			<input
				className="form-control mb-2"
				ref={telephone}
				name="telephone"
				type="text"
				placeholder="Telephone"
			/>
			<button 
				type="submit" 
				className="submit-button"
				onClick={addContact} 
			>
				Add contact
			</button>
		</form>
	);
};

export default ContactForm;
