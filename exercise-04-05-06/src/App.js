import logo from './logo.svg';
import './App.css';
// import Clock from './component/Clock';
import ClockHook from './component/ClockHook';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {/* <Clock /> */}
        <ClockHook />
      </header>
    </div>
  );
}

export default App;
