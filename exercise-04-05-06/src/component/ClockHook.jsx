import React, { useEffect, useState} from "react";
//import ‘../../styles/clock.scss’;

const ClockHook = () => {
	const initialData = {
		fecha: new Date(),
		edad: 0,
		nombre: "José",
		apellidos: "De La Cruz",
	};

	const [data, setData] = useState(initialData);

	useEffect(() => {
		const intervalAge = setInterval(() => {
			updateUser();
		}, 1000);

		return () => {
			clearInterval(intervalAge);
		};
	});

	const updateUser = () => {
		setData(() => {
			let edad = data.edad + 1;
			return {
				...data,
				fecha: new Date(),
				edad,
			};
		});
	};

	return (
		<div>
			<h3>Hora Actual - {data.fecha.toLocaleTimeString()}</h3>
			<h2>{data.nombre} {data.apellidos}</h2>
			<h1>Edad: {data.edad}</h1>
		</div>
	);
};

export default ClockHook;