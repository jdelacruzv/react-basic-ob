import logo from './logo.svg';
import './App.css';
import Contact from "./models/contact.class";
import ContactLists from './components/container/ContactLists';

function App() {
	// Intancia un objeto de la clase Contact
	const defaultContact = new Contact(
		"Jose",
		"De La Cruz",
		"jose@gmail.com",
		false
	);

	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<h1>My Contacs:</h1>
				<ContactLists contact={defaultContact}></ContactLists>
			</header>
		</div>
	);
}

export default App;
