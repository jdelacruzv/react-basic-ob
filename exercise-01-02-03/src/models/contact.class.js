class Contact {
	name = "";
	fullname = "";
	email = "";
	connected = false;

	constructor(name, fullname, email, connected) {
		this.name = name;
		this.fullname = fullname;
		this.email = email;
		this.connected = connected;
	};
};

export default Contact;