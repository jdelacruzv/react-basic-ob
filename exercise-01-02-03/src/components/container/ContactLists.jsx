import React from "react";
import ContactStatus from "../pure/ContactStatus";

const ContactLists = ({ contact }) => {
	return (
		<div>
			<h3>Name: {contact.name}</h3>
			<h3>Fullname: {contact.fullname}</h3>
			<h3>Email: {contact.email}</h3>
			<ContactStatus status={true}></ContactStatus>
		</div>
	);
};

export default ContactLists;
