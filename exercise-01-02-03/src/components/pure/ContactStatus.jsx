import React, { useState } from "react";
import PropTypes from "prop-types";

const ContactStatus = (status) => {
	const [connected, setConnected] = useState(status);

	const changeStatus = () => setConnected(!connected);

	return (
		<div>
			<h3>
				Connected: {connected === true ? "Online contact" : "Contac not available"}
			</h3>
			<button onClick={changeStatus}>{connected === true ? "Disconnected" : "Connected"}</button>
		</div>
	);
};

ContactStatus.propTypes = {
	status: PropTypes.bool
};

export default ContactStatus;
