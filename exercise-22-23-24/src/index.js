import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "react-redux";
import { createAppStore } from "./store/config/storeConfig";

const appStore = createAppStore();

ReactDOM.render(
	<Provider store={appStore}>
		<React.StrictMode>
			<App />
		</React.StrictMode>
	</Provider>,
	document.getElementById("root")
);