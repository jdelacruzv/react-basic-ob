import React, { useContext, useReducer, useRef } from "react";

// Incremental ID for Tasks
let nextTaskID = 1;

// Actions
const ADD_TASK = "ADD_TASK";
const DELETE_TASK = "DELETE_TASK";
const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";

// Context
const myContext = React.createContext(null);

const TaskList = ({ handleClick }) => {
	const stateList = useContext(myContext);

	return (
		<div className="taskList">
			<h1>Your TASKS</h1>
			<ul style={{ listStyle: "none", padding: "0", textAlign: "left" }}>
				{stateList.map((task, index) => (
					<li
						key={index}
						onClick={() =>
							handleClick({
								type: DELETE_TASK,
								payload: {
									id: task.id,
								},
							})
						}
						style={{
							textDecoration: task.deleted ? "line-through" : "none",
							textDecorationColor: task.deleted ? "green" : "none",
							color: task.deleted ? "green" : "white",
							cursor: "pointer",
						}}
					>
						{task.id} - {task.text}
					</li>
				))}
			</ul>
		</div>
	);
};

const TaskForm = ({ handleClick }) => {
	const newText = useRef();

	return (
		<div className="taskForm" style={{ marginBottom: "1rem" }}>
			<h2>Create your TASKS</h2>
			<form
				onSubmit={(e) => {
					e.preventDefault();
					handleClick({
						type: ADD_TASK,
						payload: {
							id: nextTaskID++,
							text: newText.current.value,
						},
					});
					newText.current.value = "";
				}}
			>
				<input type="text" ref={newText} />
				<button type="submit">Create Task</button>
			</form>
		</div>
	);
};

// Filter Todo List
const filterTasks = (tasks, filter) => {
	switch (filter) {
		case "SHOW_ALL":
			return tasks;
		case "SHOW_ACTIVE":
			return tasks.filter((task) => !task.completed);
		case "SHOW_COMPLETED":
			return tasks.filter((task) => task.completed);
		default:
			return tasks;
	}
};

const Filter = ({ active, handleClick, children }) => {
	if (active) {
		return <span className="active">{children}</span>;
	}

	return (
		<button
			className="filter"
			onClick={(e) => {
				e.preventDefault();
				handleClick();
			}}
		>
			{children}
		</button>
	);
};

const FilterOptions = ({ stateFilters, handleClick }) => {
	const state = useContext(myContext);

	return (
		<div className="filters">
			<Filter
				filter="SHOW_ALL"
				handleClick={filterTasks(state, stateFilters)}
			>
				ALL
			</Filter>
			<Filter
				filter="SHOW_ACTIVE"
				onClick={() =>
					handleClick({
						type: SET_VISIBILITY_FILTER,
						payload: {
							filter: filterTasks(state, stateFilters)
						},
					})
				}
			>
				ACTIVE
			</Filter>
			<Filter
				filter="SHOW_COMPLETED"
				handleClick={() => filterTasks(state, stateFilters)}
			>
				COMPLETED
			</Filter>
		</div>
	);
};

const Tasks = () => {
	// Initial state for tasksReducer
	let initialTask = [];

	// Initial state for filterReducer
	let initialFilter = "SHOW_ALL";

	// Reducer to change state of task
	const tasksReducer = (state, action) => {
		switch (action.type) {
			case ADD_TASK:
				return [
					...state,
					{
						id: action.payload.id,
						text: action.payload.text,
						deleted: false,
					},
				];
			case DELETE_TASK:
				return state.map((task) =>
					task.id === action.payload.id ? { ...task, deleted: !task.deleted } : task
				);
			default:
				return state;
		}
	};

	// Reducer to change state of filter
	const filtersReducer = (state, action) => {
		switch (action.type) {
			case SET_VISIBILITY_FILTER:
				return action.payload.filter;
			default:
				return state;
		}
	};

	// Asign useReducer to state, reducer y dispatch actions
	const [stateTasks, dispatchTasks] = useReducer(tasksReducer, initialTask);
	const [stateFilters, dispatchFilters] = useReducer(filtersReducer, initialFilter);
	console.log(stateTasks, stateFilters);

	return (
		<myContext.Provider value={stateTasks}>
			<TaskList handleClick={dispatchTasks} />
			<TaskForm handleClick={dispatchTasks} />
			{/* TODO: Falta completar la logica de todos los filtros */}
			<FilterOptions stateFilters={stateFilters} handleClick={dispatchFilters} />
		</myContext.Provider>
	);
};

export default Tasks;