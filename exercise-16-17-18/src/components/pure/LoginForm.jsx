import React from "react";
import { useNavigate } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

const LoginForm = () => {
	// Schema to validate the fields in the Formik
	const loginSchema = Yup.object().shape({
		email: Yup.string()
			.email("Invalid email format")
			.required("Email is required"),
		password: Yup.string().required("Password is required"),
	});

	const initialCredentials = {
		email: "",
		password: "",
	};

	const navigate = useNavigate();

	return (
		<div>
			<h4>Login Formik</h4>
			<Formik
				// *** Initial values that the form will take ***
				initialValues={initialCredentials}
				// *** Yup validation schema ***
				validationSchema={loginSchema}
				// *** onSubmit event ***
				onSubmit={async (values) => {
					await new Promise((r) => setTimeout(r, 1000));
					alert(JSON.stringify(values, null, 2));
					// We save the data in the localstorage
					// await localStorage.setItem("credentials", JSON.stringify(values));
					navigate("/dashboard");
				}}
			>
				{/* We obtain props from Formik */}
				{({ errors, touched, isSubmitting }) => (
					<Form>
						<label htmlFor="email">Email</label>
						<Field
							id="email"
							name="email"
							placeholder="example@email.com"
							type="email"
						/>
						{/* Email errors */}
						{errors.email && touched.email && (
							<ErrorMessage name="email" component="div" />
						)}
						<label htmlFor="password">Password</label>
						<Field
							id="password"
							name="password"
							placeholder="Password"
							type="password"
						/>
						{/* Password errors */}
						{errors.password && touched.password && (
							<ErrorMessage name="password" component="div" />
						)}
						<button type="submit">Login</button>
						{isSubmitting ? <p>Login your credentials...</p> : null}
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default LoginForm;
