import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import ROLES from "../../models/roles.enum";

const RegisterForm = ({ onSubmit }) => {
	const initialValues = {
		username: "",
		email: "",
		password: "",
		confirm: "",
		role: ROLES.USER,
	};

	const registerSchema = Yup.object().shape({
		username: Yup.string()
			.min(6, "Username too short")
			.max(12, "Username too long")
			.required("Username is required"),
		email: Yup.string()
			.email("Invalid email format")
			.required("Email is required"),
		password: Yup.string()
			.min(5, "Password too short")
			.required("Password is required"),
		confirm: Yup.string()
			.when("password", {
				is: (value) => (value && value.length > 0 ? true : false),
				then: Yup.string().oneOf([Yup.ref("password")], "¡Password must match!"),
			})
			.required("You must confirm the password"),
		role: Yup.string()
			.oneOf([ROLES.USER, ROLES.ADMIN], "You must select a role: user / admin")
			.required("Role is required"),
	});

	return (
		<div>
			<h4>Register Form</h4>
			<Formik
				// *** Initial values that the form will take ***
				initialValues={initialValues}
				// *** Yup validation schema ***
				validationSchema={registerSchema}
				// *** onSubmit event ***
				onSubmit={async (values) => {
					await new Promise((r) => setTimeout(r, 1000));
					// eslint-disable-next-line no-undef
					onSubmit(values);
					alert("Register user");
				}}
			>
				{({ errors, touched, isSubmitting }) => (
					<Form>
						<label htmlFor="username">Username</label>
						<Field
							id="username"
							name="username"
							placeholder="Your username"
							type="text"
						/>
						{/* Username errors */}
						{errors.username && touched.username && (
							<ErrorMessage name="username" component="div" />
						)}
						<label htmlFor="email">Email</label>
						<Field
							id="email"
							name="email"
							placeholder="example@email.com"
							type="email"
						/>
						{/* Email errors */}
						{errors.email && touched.email && (
							<ErrorMessage name="email" component="div" />
						)}
						<label htmlFor="password">Password</label>
						<Field
							id="password"
							name="password"
							placeholder="Password"
							type="password"
						/>
						{/* Password errors */}
						{errors.password && touched.password && (
							<ErrorMessage name="password" component="div" />
						)}
						<label htmlFor="confirm">Confirm password</label>
						<Field
							id="confirm"
							name="confirm"
							placeholder="Confirm password"
							type="password"
						/>
						{/* Confirm password errors */}
						{errors.confirm && touched.confirm && (
							<ErrorMessage name="confirm" component="div" />
						)}
						<button type="submit">Register account</button>
						{isSubmitting ? <p>Sending your credentials...</p> : null}
					</Form>
				)}
			</Formik>
		</div>
	);
};

export default RegisterForm;
