import React from "react";
import PropTypes from "prop-types";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { LEVELS } from "../../models/levels.enum";
import { Task } from "../../models/task.class";

const TaskFormik = ({ add, numTask }) => {
	const taskSchema = Yup.object().shape({
		name: Yup.string()
			.min(5, "Task too short")
			.max(50, "Task too long")
			.required("Task is required"),
		description: Yup.string()
			.min(5, "Description too short")
			.max(50, "Description too long")
			.required("Description is required"),
	});

	const initialValues = {
		name: "",
		description: "",
		done: false,
		level: LEVELS.NORMAL
	};

	const addTask = (values, actions) => {
		setTimeout(() => {
      const newTask = new Task(
				values.name, 
				values.description, 
				values.done,
				values.level,
			);
      actions.resetForm({});
      actions.setSubmitting(false);
			add(newTask);
    }, 500);
  }


	return (
		<div>
			<Formik
				// *** Initial values that the form will take ***
				initialValues={initialValues}
				// *** Yup validation schema ***
				validationSchema={taskSchema}
				// *** onSubmit event ***
				onSubmit={addTask}
			>
				{({ errors, touched, isSubmitting }) => (
					<Form>
						<Field
							type="text"
							name="name"
							className="form-control form-control-md"
							placeholder="Task name"
						/>
						{/* Name errors */}
						{errors.name && touched.name && (
							<ErrorMessage name="name" component="div" />
						)}
						<Field
							type="text"			
							name="description"
							className="form-control form-control-md"
							placeholder="Task description"
						/>
						{/* Description errors */}
						{errors.description && touched.description && (
							<ErrorMessage name="description" component="div" />
						)}
						<Field as="select" name="level" className="form-control form-control-md">
							<option value={LEVELS.NORMAL}>Normal</option>
							<option value={LEVELS.URGENT}>Urgent</option>
							<option value={LEVELS.BLOCKING}>Blocking</option>
						</Field>
						<button type="submit" className="btn btn-success mt-3">
							{numTask === 0 ? "Create your first task" : "Add new task"}
						</button>
						{isSubmitting ? <p>Sending your task...</p> : null}
					</Form>
				)}
			</Formik>
		</div>
	);
};

TaskFormik.protoTypes = {
	add: PropTypes.func.isRequired,
	numTask: PropTypes.number.isRequired,
};

export default TaskFormik;
