import React, { useEffect, useState } from "react";
import { LEVELS } from "../../models/levels.enum";
import { Task } from "../../models/task.class";
import TaskComponent from "../pure/TaskComponent";
import TaskFormik from "../pure/TaskFormik";

const TaskList = ({ user }) => {
	const defaultTask1 = new Task("Example1", "Description1", true, LEVELS.NORMAL);
	const defaultTask2 = new Task("Example2", "Description2", false, LEVELS.URGENT);
	const defaultTask3 = new Task("Example3", "Description3", false, LEVELS.BLOCKING);

	// Estado del componente
	const [tasks, setTasks] = useState([defaultTask1, defaultTask2, defaultTask3]);
	const [loading, setLoading] = useState(true);

	// Control del ciclo de vida del componente
	useEffect(() => {
		console.log("Task state has been modified");
		setTimeout(() => {
			setLoading(false);
		}, 2000);

		return () => {
			console.log("TaskList component is going to unmount...");
		};
	});

	const completeTask = (task) => {
		console.log("Complete this Task:", task);
		const index = tasks.indexOf(task);
		const tempTasks = [...tasks];
		tempTasks[index].complete = !tempTasks[index].complete;
		// We update the state of the component with the new list of tasks and it
		// will update the iteration of the tasks	in order to show the tasks updated
		setTasks(tempTasks);
	};

	const deleteTask = (task) => {
		console.log("Delete this Task:", task);
		const index = tasks.indexOf(task);
		const tempTasks = [...tasks];
		tempTasks.splice(index, 1);
		setTasks(tempTasks);
	};

	const addTask = (task) => {
		console.log("Add this Task:", task);
		const tempTasks = [...tasks];
		tempTasks.push(task);
		setTasks(tempTasks);
	};

	const TasksTable = () => {
		return (
			<div>
				<h1>Task List - Usuario: {user.email}</h1>
				<table>
					<thead>
						<tr>
							<th scope="col">Title</th>
							<th scope="col">Description</th>
							<th scope="col">Priority</th>
							<th scope="col">Actions</th>
						</tr>
					</thead>
					<tbody>
						{tasks.map((task, index) => {
							return (
								<TaskComponent
									key={index}
									task={task}
									complete={completeTask}
									remove={deleteTask}
								/>
							);
						})}
					</tbody>
				</table>
			</div>
		);
	};

	let tasksTable;

	if (tasks.length > 0) {
		tasksTable = <TasksTable />;
	} else {
		tasksTable = (
			<div>
				<h3>There are not tasks to show</h3>
				<h4>Please, create one...</h4>
			</div>
		);
	}

	const loadingStyle = {
		color: "gray",
		fontSize: "30px",
		fontWeight: "bold"
	};

	return (
		<div>
			<div className="col-12">
				<div className="card">
					<div className="card-header p-3">
						<h5>Your Task:</h5>
					</div>
					<div 
						className="card-body" 
						data-mdb-perfect-scrollbar="true"
						style={{position: "relative", height: "400px"}}
					>
						{/* TODO: Add loading spinner */}
						{loading ? <p style={loadingStyle}>loading tasks...</p> : tasksTable}						
					</div>
				</div>
			</div>
			<TaskFormik add={addTask} numTask={tasks.length} />
		</div>
	);
};

export default TaskList;
