import React, { useState } from "react";
import { getRandomJoke } from "../services/axiosService";
import Button from "@mui/material/Button";
import style from "./Joke.module.css";

const Joke = () => {
	const [joke, setJoke] = useState("");
	const [like, setLike] = useState(0);
	const [dislike, setDislike] = useState(0);

	const obtainNewJoke = () => {
		getRandomJoke()
			.then((response) => {
				if (response.status === 200) {
					setJoke(response.data);
				}
			})
			.catch((error) => {
				console.error(`Something went wrong: ${error}`);
			});
	};

	return (
		<div>
			<div className={style.img__image}>
				<img src="../../chucknorris_logo.png"	alt="chuck-norris-logo" width="150px"	/>
			</div>
			<div className={style.btn__random}>
				<Button variant="contained" color="success" onClick={obtainNewJoke}>
					Random joke
				</Button>
			</div>
			<div className={style.div__jokes}>
				{joke === "" ? <p>Generate a new joke</p> : <p>"{joke.value}"</p>}
			</div>
			{joke === "" ? null : (
				<div className={style.btn__scores}>
					<div className={style.btn__likes}>
						<Button variant="contained" size="large" onClick={() => setLike(like + 1)}>
							Like
						</Button>
						<Button variant="outlined" size="large" color="error" onClick={() => setDislike(dislike + 1)}>
							Dislike
						</Button>
					</div>
					<div className={style.btn__record}>
						<p>RECORD:</p>
						<p>LIKE: {like}</p>
						<p>DISLIKE: {dislike}</p>
					</div>
				</div>
			)}
		</div>
	);
};

export default Joke;
