import './App.css';
import Joke from "./components/Joke.jsx"

function App() {
  return (
    <div className="App">
      <Joke />
    </div>
  );
}

export default App;
