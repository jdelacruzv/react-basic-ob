import ApiRequest from "../utils/axios.config";

export function getRandomJoke() {
	return ApiRequest.get("/", {
		validateStatus: function (status) {
			return status < 500; // Resolve only if the status code is less than 500
		},
	}); // https://api.chucknorris.io/jokes/random/
}
