import React, { useState } from "react";

const Square = () => {
	const [color, setColor] = useState("#000000");
	const [timer, setTimer] = useState(0);

	// Get random hex color
	const getColorHex = () => {
		const colorHex = `#${Math.floor(Math.random() * 16777215).toString(16)}`;
		return setColor(colorHex);
	};

	const convertRGBtoHex = (getColorHex) => {
		const hex = getColorHex;
		const red = parseInt(hex[1] + hex[2], 16);
		const green = parseInt(hex[3] + hex[4], 16);
		const blue = parseInt(hex[5] + hex[6], 16);
		return `${red}, ${green}, ${blue}`;
	};

	const onChangeColor = () => {
		return setTimer(setInterval(getColorHex, 1000));
	};

	const onStopChangeColor = () => {
		return clearInterval(timer);
	};

	const onDoubleClickChangeColor = () => {
		return clearInterval(timer);
	};

	const squareStyle = {
		width: "255px",
		height: "255px",
		cursor: "pointer",
		backgroundColor: color,
	};

	const textStyle = {
		display: "flex",
		alignItems: "center",
		textAlign: "center",
		fontWeight: "bold",
		fontSize: "1rem",
		margin: ".5rem 0 0 0",
	};

	const colorStyle = {
		color: "#3f3844",
		marginLeft: ".5rem",
	};

	return (
		<>
			<div 
				style={squareStyle} 
				onMouseOver={onChangeColor} 
				onMouseLeave={onStopChangeColor} 
				onDoubleClick={onDoubleClickChangeColor}>
			</div>
			<p style={textStyle}>
				HEX <span style={colorStyle}>{color.toUpperCase()}</span>
			</p>
			<p style={textStyle}>
				RGB <span style={colorStyle}>{convertRGBtoHex(color).toUpperCase()}</span>
			</p>
		</>
	);
};

export default Square;
